#include <iostream>
#include <vector>
#include <numeric>
#include <limits>

using namespace std;


vector<int> solution;

void calculate(int numberOfParticipants, int price, vector<int> budgets) {
//    cout << 2323 << endl;
    int min = budgets[0];
    int indexOfMin = 0;
    bool changed = false;
    if (numberOfParticipants > 1) {
        for (int i = 0; i < budgets.size(); ++i) {
            if (budgets[i] < min && budgets[i] != -1) {
                min = budgets[i];
                indexOfMin = i;
                changed = true;
                budgets[i] = -1;
    //            cout << min << endl;
            }
        }
    }
    if (changed) {
//        cout << 1132133 <<endl;
//        cout << indexOfMin << endl;
//        cout << solution.at(indexOfMin);
//        cout << min <<endl;
        solution[indexOfMin] = min;
//        cout << 43434 <<endl;
        calculate(numberOfParticipants -1, price - min, budgets);
    } else {
        int each = price / numberOfParticipants;
//        cout <<price <<endl;
//        cout <<each <<endl;
        int lastIndex;
        for (int i = 0; i < budgets.size(); ++i) {
            if (budgets[i] != -1) {
                solution[i] = each;
                lastIndex = i;
            }
        }
        solution[lastIndex] = price - each * (numberOfParticipants -1);
    }
}


int main() {
    int N;
    int C;
    vector<int> budgets;

    cin >> N; cin.ignore();
    cin >> C; cin.ignore();
    for (int i = 0; i < N; ++i) {
        int B;
        cin >> B; cin.ignore();
        budgets.emplace_back(B);
    }

    int total = accumulate(budgets.begin(), budgets.end(), 0);
    if (total < C) {
        cout << "IMPOSSIBLE" << endl;
        return 0;
    }
    solution = budgets;
    calculate(N, C, budgets);
//    cout << endl;
    for (int b : solution) {
        cout << b << endl;
    }
}